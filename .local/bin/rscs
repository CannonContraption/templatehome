#!/bin/dash

RSYNCCSTATIONRC="$HOME/.config/rscs/rsynccstation.rc"

if [ ! -d "$HOME/.config/rscs" ]; then
    mkdir $XDG_CONFIG_HOME/rscs
    ln -s $XDG_CONFIG_HOME/vanilla/ln/.config/rscs/rsynccstation.rc $XDG_CONFIG_HOME/rscs
    ln -s $XDG_CONFIG_HOME/vanilla/ln/.config/rscs/rsexcludes $XDG_CONFIG_HOME/rscs
    ln -s $XDG_CONFIG_HOME/vanilla/ln/.config/rscs/rstargetlist $XDG_CONFIG_HOME/rscs
fi

. $RSYNCCSTATIONRC

upload="yes"
download="yes"

target="" # Default target to sync (Default, NONE)

# Excludes RSync Arguments
excludes="--exclude-from $path_to_excludes" #if you prefer to hard-code
#exludes, use this variable instead of the excludes file

# RSync Options
rsoptions="-avu --progress=info2"
# DEFAULT OPTIONS:
#  -a              | archive (-rlptgoD)
#  -v              | verbose
#  -u              | update (skip newer files)
# --progress=info2 | Show file sync progress

# Targets list. Pretty-prints all targets.
targetslist()
{
    awk -F ';;;;;' '{print $1"\t\t"$2}' $source_path_file
    exit
}

# Help dialog. Shows the rsynccstation help page.
helpdialog()
{
    echo '\033[32m'
    echo '.__  ._  _ _ __|_ _._|_o _ ._'
    echo '|_>\/| |(_(__> |_(_| |_|(_)| |'
    echo '   / 2016-19 CannonContraption'
    echo '\033[m'
    echo 'USEAGE: rsynccstation [options] [targets]'
    echo
    echo 'options:'
    echo '\033[1;34m  -h            \033[33m| Show this help screen'
    echo '\033[1;34m  -t            \033[33m| Show list of targets'
    echo '\033[1;34m  -up           \033[33m| Only upload files, not download'
    echo '\033[1;34m  -dn           \033[33m| Only download files, not upload'
    echo '\033[1;34m  -s (server)   \033[33m| Specify a server'
    echo '\033[1;34m  -p (port)     \033[33m| Specify a port'
    echo '\033[1;34m  -u (username) \033[33m| Specify a username\033[m'
    exit
}

# Argument parsing.
# For this new version of rscs, we're using actual argument parsing, rather than
# using some static syntax.
sshcommand="ssh"
while [ $# != 0 ]; do
    case $1 in
        "-p" ) shift;
               portnumber="$1" ;;
        "-s" ) shift;
               server="$1" ;;
        "-u" ) shift;
               username="$1" ;;
        "-h" ) helpdialog ;;
        "-t" ) targetslist ;;
        "-up" ) unset download ;;
        "-dn" ) unset upload ;;
        *    ) if [ -n "$target" ]; then
                   target="$target;;;;;$1"
               else
                   target="$1"
               fi ;;
    esac
    if [ $# = 0 ]; then
        echo "You are missing a parameter."
        exit 1
    fi
    shift
done

sshcommand="ssh -p $portnumber"

split_excludes()
{
    while [ $# -gt 0 ]; do
        targetexcludes="$targetexcludes --exclude $1"
        shift
    done
    echo "$targetexcludes"
}

sync_one()
{
    # Automatic parameters
    source_name=$(grep -E "^$@;;;;;" $source_path_file | awk -F ';;;;;' '{print $1}')
    if [ "$source_name" != "$@" ]; then
        echo "Path to \"$@\" not found!"
        return 1
    fi
    source_path=$(grep -E "^$@;;;;;" $source_path_file | awk -F ';;;;;' '{print $2}')
    dest_path=$(echo $source_path | awk -F '/' '{OFS="/"; $(NF)=""; print $0}')
    echo "\033[7;33m$source_name\033[m"
    # Figure out excludes...
    target_excludes="$(grep -E "^$@;;;;;" $source_path_file | awk -F ';;;;;' '{print $3}')"
    if [ -n "$target_excludes" ]; then
        target_excludes=$(split_excludes $target_excludes)
    fi
    # From here down is the code to sync stuff itself.
    # Start with an rsync pull operation. Bring in new files first.
    if [ -n "$download" ]; then
        rsync $rsoptions $excludes $target_excludes -e "$sshcommand" $username@$server:$source_path $dest_path
    fi
    if [ $? = 0 ]; then
	echo "\033[1;32mPull complete. Beginning push...\033[m"
    else
	echo "\033[1;31mFailed to fetch files.\033[m Quit."
	exit 2;
    fi

    # Then send any locally changed files.
    if [ -n "$upload" ]; then
        rsync $rsoptions $excludes $target_excludes -e "$sshcommand" $source_path $username@$server:$dest_path
    fi
    if [ $? = 0 ]; then
	echo "\033[1;32mPush complete. Script is successful.\033[m"
    else
	echo "\033[1;31mFailed to push files.\033[m Quit."
	exit 3;
    fi
}

while [ -n "$target" ]; do
    sync_one $(echo $target | awk -F ';;;;;' '{print $(NF)}')
    target=$(echo $target | awk -F ';;;;;' '{OFS=";;;;;"; NF--; print $0}')
done
