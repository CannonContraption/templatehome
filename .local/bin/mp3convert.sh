#!/bin/sh

originalversion=$@
mp3version=$(echo $originalversion | awk -F '.' '{OFS="."; $(NF)="mp3"; print $0}')
echo $mp3version
artist=$(pwd | awk -F '/' '{print $(NF-1)}')
album=$(pwd | awk -F '/' '{print $(NF)}')
mkdir --parents "$HOME/Audio/convert/mp3/$artist/$album"
ffmpeg -i "$originalversion" "$HOME/Audio/convert/mp3/$artist/$album/$mp3version"
