export PATH=$HOME/.local/bin/$(hostname):$HOME/.local/bin/$(uname -m):$HOME/.local/bin:/usr/local/bin:/usr/local/sbin:$PATH

############# CONFIG DIR SETTINGS
if [ -z "$XDG_RUNTIME_DIR" ]; then
    # Void doesn't set this since it doesn't use systemd
    export XDG_RUNTIME_DIR="/dev/shm/$USER-run-$(date '+%M%s')"
fi

# Nothing else seems to set these for some reason
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

# APPLICATION OVERRIDES
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export DVDCSS_CACHE="$XDG_CACHE_HOME/dvdcss"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export JUPYTER_CONFIG_DIR=$XDG_CONFIG_HOME/jupyter
export LESSKEY="$XDG_CONFIG_HOME/less/lesskey"
export LESSHISTFILE="$XDG_CACHE_HOME/lesshst"
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME"/mail/notmuchrc
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/password-store
export TASKRC="$XDG_CONFIG_HOME/task/taskrc"
export VIMINIT=":source $XDG_CONFIG_HOME"/vim/init.vim
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

############# DEFAULT APPLICATIONS
export BROWSER='firefox'
export browser=$BROWSER
export textbrowser='w3m'
export EDITOR='nvim'
export TERMINAL='alacritty'

which $TERMINAL > /dev/null 2>&1 ||
{
    which xterm && TERMINAL=xterm
    which urxvt && TERMINAL=urxvt
    which st    && TERMINAL=st
}

############# Platform overrides

[ -e /opt/profile-site ] && . /opt/profile-site

############# SSH agent
systemctl start --user ssh-agent

