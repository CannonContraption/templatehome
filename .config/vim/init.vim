set undodir=$XDG_DATA_HOME/vim/undo
set directory=$XDG_DATA_HOME/vim/swap
set backupdir=$XDG_DATA_HOME/vim/backup
set viminfo+='1000,n$XDG_DATA_HOME/vim/viminfo
set runtimepath=$XDG_CONFIG_HOME/vim,$VIMRUNTIME,$XDG_CONFIG_HOME/vim/after
" Syntax is important.
syntax on

" And line numbering.
set nu

" I like auto-indent, so that's in too
set ai
" And C-Style strict indentation is nice
au FileType c setlocal cin

" Sets our tabstop to 8
"set tabstop=4
set tabstop=8
" And our indent size to 4
set sw=8

" shows all command stuff, so displays when you hit ^[
set showcmd

"set expandtab "Spaces indent
set noexpandtab

" Shows an underline under the currently selected line
"set cursorline
" Disabled since it causes issues on some setups

" Enables a completions menu
set wildmenu

" Stops Vim from redrawing when it needs not must
set lazyredraw

" Incremental search that updates as you type, and highlights everything
set incsearch
set hlsearch

"set mouse=a

" Workman is a great layout, but not for Vim normal mode
" let g:workman_normal_qwerty = 1

" The default color scheme is hard to read in LXterminal and similar, change it to something easier for standard grey-on-black terminals
color delek

" Git commits are easier to handle if they wrap for you!
au FileType gitcommit setlocal tw=72

" Email customizations
au FileType mail setlocal tw=72
au FileType mail setlocal sw=8
au FileType mail setlocal noai
