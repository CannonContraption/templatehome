################################################################################
# Shell Settings
################################################################################

HISTFILE=$XDG_CONFIG_HOME/zsh/.histfile
HISTSIZE=4000
SAVEHIST=4000
setopt appendhistory beep extendedglob
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '$HOME/.zshrc'

bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line
autoload -Uz compinit
compinit
setopt histignoredups histignorespace
if [ -e ~/.config/zsh/sessionincludes ]; then
	source ~/.config/zsh/sessionincludes
fi

setopt promptsubst

################################################################################
# Built-in functions
################################################################################

# bk - background a command and suppress its output
bk() {
	$@ > /dev/null 2>&1 &
}

fswapin-new-files ()
{
    if [ ! -d $1 ]; then
        fsi $1 || {
            mkdir --parents $1
            nfswap add rsync $1
            fsi $1
        }
    fi
}

gpto()
{
    branch=$1
    shift
    git push $branch $(git rev-parse --abbrev-ref HEAD) $@
}

# Dev mode - enables/disables extra prompt settings, and optionally a dev env
#            if such a thing exists. Tracks its state through $devmode_enabled
#            so subsequent runs of the devmode command toggle its state.
export devmode_enabled="no"
devmode() {
	if [ $devmode_enabled = "no" ]; then
		PS1=$': %{\033[36m%}$(history -n -1)%{\033[m%}\n'$PS1
		if [ -e env/bin/activate ]; then
			source env/bin/activate
		fi
		export devmode_enabled="yes"
	else
		which deactivate > /dev/null 2>&1 &&
		deactivate
		PS1=$(echo $PS1 | tail -n 2)$'\n'
		export devmode_enabled="no"
	fi
}

################################################################################
# Shell Prompt
################################################################################

lastreturn() {
	RCODE=$?
	if [ $RCODE = 0 ]; then
		echo -ne '%{\033[7;32m%}  0 %{\033[m%} %{\033[7m%}'
	else
		echo -ne "%{\033[7;31m%}"
		printf "%03s" $RCODE
		echo -ne " %{\033[m%} %{\033[7m%}"
	fi
}

getgitbranch() {
	git rev-parse > /dev/null 2>&1
	if [ $? = 0 ]; then
		CHANGEDFILES=$(git status -s 2>/dev/null | wc -l)
		echo -ne "%{\033[m\033[32m%} GIT %{\033[0;4m%}"
		echo -n $(git branch | sed -n '/\* /s///p')
		if [ $CHANGEDFILES != 0 ]; then
			echo -ne "%{\033[0;1;31m%} "
			echo -n $CHANGEDFILES
		fi
		echo -ne '%{\033[m%}'
	fi
}

PROMPT=$'`lastreturn`%n@%m%{\033[0m%} %{\e[1;34m%}%~%{\e[m%} %{\e[33m%}$(date +%H:%M)%{\e[m%}`getgitbranch`\n'

# Enable syntax highlighting (if available)
[ -e /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] &&
   source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Arch can't leave a path alone, source its zsh-syntax-highlighting if it
# exists
[ -e /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] &&
   source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


################################################################################
# Aliases
################################################################################

# exa doesn't have the -Z flag for selinux, so don't make regular ls point there
# even though it's generally better. ll and la are shortcuts anyhow, so whatever
alias ls='ls --color=auto'
if which exa > /dev/null; then
	alias ll='exa -l'
	alias la='exa -la'
else
	alias ll='ls -l'
	alias la='ls -la'
fi

alias gitst='git status -s'

################################################################################
# Environment Settings
################################################################################

export EDITOR='vim'
if [ -z ${SSH_AUTH_SOCK+x} ]; then
	export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
fi
precmd(){
	echo -ne "\033]0;Z Shell | $USER@$HOSTNAME:$PWD ($TERM)\007"
}
preexec(){
    echo -ne "\033]0;$1 | $USER@$HOSTNAME:$PWD ($TERM)\007"
}
