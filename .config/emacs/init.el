;; init.el
;;
;; Jim Read's init.el file
;; Written over a span of years, starting sometime in mid-2017.

;; Just so we have all the load dirs...
(add-to-list 'load-path "~/.config/emacs/lisp/")

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; This file got really big within the first year+ or so, so some bits of the
;; file were spun off into sub-files, and given named features to enable. so for
;; example, customizations to C-mode and LaTeX handling are in
;; lang-mode-customizations, but since I'm a heavy org-mode user, that has its
;; own file.
(require 'org-customizations-file)
(require 'lang-mode-customizations)
(require 'macros-customization)

(require 'package)
(add-to-list
 'package-archives
 '("melpa-stable" . "http://melpa.org/packages/")
 t)

(require 'xah-fly-keys)
;(xah-fly-keys-set-layout 'workman) ; I'm assuming you don't use workman, uncomment if so
(xah-fly-keys-set-layout 'qwerty)
(xah-fly-keys 1)

;; And I use multiple cursors a lot, I mean a *LOT*, so here that is
(require 'multiple-cursors)
(global-set-key (kbd "<mouse-8>") 'mc/add-cursor-on-click)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C-S-c C-S-<mouse-1>") 'mc/add-cursor-on-click)

;; I used to like window transparency. Therefore, here it is:
;(set-frame-parameter (selected-frame) 'alpha '(90 . 90))
;(add-to-list 'default-frame-alist '(alpha . (90 . 90)))

;; The toolbar and menubar just get in my way. Therefore they are removed. So is
;; the scrollbar. So is the startup screen.
(setq inhibit-startup-screen t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(set-scroll-bar-mode nil)

;; Now some default behaviors...
(setq tab-always-indent 'complete)
(setq-default indent-tabs-mode nil)
(setq indent-tabs-mode nil)

;; And I was lazy and used the default font picker. It means the config file is
;; much larger than it realistically needs to be, but at least I got the font I
;; wanted here, so whatever.
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Fira Mono" :foundry "CTDB" :slant normal :weight normal :height 83 :width normal)))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(c-tab-always-indent 'other)
 '(custom-enabled-themes '(tsdh-light))
 '(frame-resize-pixelwise t)
 '(indent-tabs-mode nil)
 '(ledger-reports
   '(("test" "ledger bal")
     ("bal" "%(binary) -f %(ledger-file) bal")
     ("reg" "%(binary) -f %(ledger-file) reg")
     ("payee" "%(binary) -f %(ledger-file) reg @%(payee)")
     ("account" "%(binary) -f %(ledger-file) reg %(account)")))
 '(menu-bar-mode nil)
 '(package-selected-packages
   '(markdown-mode magit kotlin-mode xah-fly-keys ledger-mode gdscript-mode web-mode s rust-mode multiple-cursors undo-tree))
 '(send-mail-function 'sendmail-send-it)
 '(tab-always-indent 'complete)
 '(tool-bar-mode nil))
