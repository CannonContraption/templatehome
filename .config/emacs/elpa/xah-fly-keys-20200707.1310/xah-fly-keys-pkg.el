;;; -*- no-byte-compile: t -*-
(define-package "xah-fly-keys" "20200707.1310" "ergonomic modal keybinding minor mode." '((emacs "24.1")) :commit "8c9c4df25e1406b093a32c87da19803337e4e09c" :keywords '("convenience" "emulations" "vim" "ergoemacs") :authors '(("Xah Lee ( http://xahlee.info/ )")) :maintainer '("Xah Lee ( http://xahlee.info/ )") :url "http://ergoemacs.org/misc/ergoemacs_vi_mode.html")
