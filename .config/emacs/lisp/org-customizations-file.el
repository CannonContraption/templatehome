(provide 'org-customizations-file)

;; I use org-mode *constantly*, so I have a few tweaks here that make it work
;; for me exactly as I like it.
(setq org-agenda-files '("~/doc/task/"))
(setq org-directory "~/doc/task")
(setq diary-file (concat org-directory "/diary"))
(setq org-agenda-include-diary t)
(setq org-modules (quote (org-habit)))
(global-set-key "\C-ca" 'org-agenda)

(setq org-default-notes-file (concat org-directory "/inbox.org"))
(setq org-todo-keywords '((sequence "TODO(t)" "WAITING(w@/!)" "|" "DONE(d@/!)" "CANCELLED(c@/!)")))
(setq org-log-into-drawer "LOGBOOK")
(setq org-log-done 'time)

(setq org-agenda-custom-commands
      '(("W" todo-tree "WAITING")
        ("a" "Full Agenda and Nexts"
         ((agenda "")
          (tags-todo "+next")
          (tags-todo "+inbox")))
        ("N" tags-todo "+next")))

(setq org-refile-targets '(("~/doc/task/example1.org" :maxlevel . 2)
                           ("~/doc/task/example2.org" :maxlevel . 2)))

(defun diary-batch (&optional ndays)
  (interactive "P")
  (let ((diary-print-entries-hook (lambda () (princ (buffer-string))))
	(diary-display-function 'diary-fancy-display))
    (diary-list-entries (calendar-current-date) 1)
    (diary-print-entries)))
                           

(require 'ox-latex)
(add-to-list 'org-latex-classes
             '("proc"
               "\\documentclass[11pt]{proc}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

