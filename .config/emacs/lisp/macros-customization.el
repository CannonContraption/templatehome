(provide 'macros-customization)

(defun write-new-journal-entry ()
  "Open a text journal, dated to specified date (default today).

Creates a new entry and allows the user to then type. Should act as an
all-in-one macro for journaling, including inserting the relevant date
information."
  (interactive)
  (find-file "~/doc/wri/journal.org")
  (goto-char (point-max))
  (insert (concat "* " (org-read-date) "\n")))
